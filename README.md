# Course Search Engine
Course Search Engine is a spring boot application which has two end points. 


## Building project
```bash
mvn clean install 
```

## End points
Following end point is searching in the stub data  
```bash    
    /courses/search/{term}
```
Following end point is searching in the stub data and returning data with total time 
```bash    
    /courses/search/{term}/totaltime
```

Because there is only one parameter, I used path variable instead of query string.
