package com.bmj.course.controller;

import java.util.List;

import com.bmj.course.model.Course;
import com.bmj.course.service.CourseDataService;
import com.bmj.course.viewmodel.CourseTotalTimeView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CourseController {

    @Autowired
    CourseDataService courseDataService;

    @GetMapping("/courses/search/{term}")
    public ResponseEntity<List<Course>> getFilteredList(@PathVariable(value = "term") String term) {

        final var courses = courseDataService.searchTerm(term);
        if (courses.size() > 0) {
            return ResponseEntity.ok().body(courses);
        }

        return ResponseEntity.notFound().build();
    }

    @GetMapping("/courses/search/{term}/totaltime")
    public ResponseEntity<CourseTotalTimeView> getFilteredListWithTotalTime(@PathVariable(value = "term") String term) {
        return courseDataService.searchTermWithTotalTime(term)
                .map(courseTotalTimeView -> ResponseEntity.ok().body(courseTotalTimeView))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

}
