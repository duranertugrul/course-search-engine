package com.bmj.course.model;

public class Course {
    private double courseTime;
    private String courseName;

    public Course(final double courseTime, final String courseName) {
        this.courseTime = courseTime;
        this.courseName = courseName;
    }

    public double getCourseTime() {
        return courseTime;
    }

    public void setCourseTime(final double courseTime) {
        this.courseTime = courseTime;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(final String courseName) {
        this.courseName = courseName;
    }
}
