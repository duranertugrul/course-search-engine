package com.bmj.course.repository;

import static java.util.Arrays.asList;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class CourseDataRepository {
    private List<String> courseData = asList("5.00,Introduction to mechanical ventilation",
            "3.5,Introduction to coronavirus disease 2019 (COVID-19)",
            "2.00,Clinical pointers: COVID-19 in primary care",
            "1,Clinical pointers: remote consultations in primary care",
            "Quick tips: introduction to asthma",
            "10.25,Infection control - including basic personal protective equipment",
            "7.50,Introduction to testing for COVID-19",
            "2.00,Airways management: tracheal intubation",
            "2.50,Quick tips: proning in critical care",
            "3.0,Quick tips: introduction to asthma");

    public List<String> getCourseData(){
        return courseData;
    }

}