package com.bmj.course.viewmodel;

import java.util.List;

import com.bmj.course.model.Course;

public class CourseTotalTimeView {
    private double totalCourseTime;
    private List<Course> courses;

    public CourseTotalTimeView(final double totalCourseTime, final List<Course> courses) {
        this.totalCourseTime = totalCourseTime;
        this.courses = courses;
    }

    public double getTotalCourseTime() {
        return totalCourseTime;
    }

    public void setTotalCourseTime(final double totalCourseTime) {
        this.totalCourseTime = totalCourseTime;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(final List<Course> courses) {
        this.courses = courses;
    }
}
