package com.bmj.course.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.bmj.course.model.Course;
import com.bmj.course.repository.CourseDataRepository;
import com.bmj.course.viewmodel.CourseTotalTimeView;
import org.springframework.stereotype.Service;

@Service
public class CourseDataService {

    private CourseDataRepository courseDataRepository;


    public CourseDataService(final CourseDataRepository courseDataRepository) {
        this.courseDataRepository = courseDataRepository;
    }


    public List<Course> searchTerm(final String term) {

        return courseDataRepository
                .getCourseData()
                .stream()
                .map(data -> {
                    String[] dataArray = data.split(",");
                    if (dataArray.length > 1) {
                        return new Course(Double.parseDouble(dataArray[0]), dataArray[1]);
                    } else {
                        return new Course(0, dataArray[0]);
                    }

                })
                .filter(course -> course.getCourseName().toLowerCase().contains(term.toLowerCase()))
                .collect(Collectors.toList());
    }


    public Optional<CourseTotalTimeView> searchTermWithTotalTime(final String term) {
        final var courses = searchTerm(term);

        if (courses.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(new CourseTotalTimeView(
                courses
                        .stream()
                        .map(Course::getCourseTime).mapToDouble(Double::doubleValue).sum(),
                courses));
    }
}
