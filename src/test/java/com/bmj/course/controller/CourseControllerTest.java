package com.bmj.course.controller;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Optional.of;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bmj.course.model.Course;
import com.bmj.course.service.CourseDataService;
import com.bmj.course.viewmodel.CourseTotalTimeView;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(CourseController.class)
public class CourseControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CourseDataService courseDataService;

    @Test
    public void shouldReturnCourseFromServiceWhenFound() throws Exception {
        when(courseDataService.searchTerm("mechanical")).thenReturn(asList(new Course(5, "Introduction to mechanical ventilation")));
        this.mockMvc.perform(get("/courses/search/mechanical"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("[0].courseTime").value(5))
                .andExpect(jsonPath("[0].courseName").value("Introduction to mechanical ventilation"));
    }

    @Test
    public void shouldReturnCourseFromServiceWhenNotFound() throws Exception {
        when(courseDataService.searchTerm("test123")).thenReturn(emptyList());
        this.mockMvc.perform(get("/courses/search/test123/totaltime"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }


    @Test
    public void shouldReturnCourseWithTotalTimeFromServiceWhenFound() throws Exception {
        CourseTotalTimeView result = new CourseTotalTimeView(5.5,
                asList(new Course(1, "Quick tips: introduction to asthma"),
                        new Course(2, "Quick tips: proning in critical care"),
                        new Course(3, "Quick tips: introduction to asthma")));
        when(courseDataService.searchTermWithTotalTime(any())).thenReturn(of(result));
        this.mockMvc.perform(get("/courses/search/quick/totaltime"))
                .andDo(print())
                .andExpect(jsonPath("totalCourseTime").value(5.5))
                .andExpect(jsonPath("courses.[0].courseName").value("Quick tips: introduction to asthma"))
                .andExpect(jsonPath("courses.[1].courseName").value("Quick tips: proning in critical care"))
                .andExpect(jsonPath("courses.[2].courseName").value("Quick tips: introduction to asthma"));
    }

}
