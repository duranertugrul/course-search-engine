package com.bmj.course.service;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import com.bmj.course.model.Course;
import com.bmj.course.repository.CourseDataRepository;
import com.bmj.course.viewmodel.CourseTotalTimeView;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CourseDataServiceTest {

    @Mock
    CourseDataRepository repository;

    @InjectMocks
    CourseDataService underTest;

    @BeforeEach
    public void setup() {
        when(repository.getCourseData()).thenReturn(asList("5.00,Introduction to mechanical ventilation",
                "3.5,Introduction to coronavirus disease 2019 (COVID-19)",
                "Quick tips: introduction to asthma",
                "7.50,Introduction to testing for COVID-19",
                "3.0,Quick tips: introduction to asthma"));
        underTest = new CourseDataService(repository);
    }

    @Test
    public void shouldReturnList_WhenItSingleMatched() {
        List<Course> courseList = underTest.searchTerm("mechanical");
        assertEquals(1, courseList.size());
        assertEquals("Introduction to mechanical ventilation", courseList.get(0).getCourseName());
        assertEquals(5, courseList.get(0).getCourseTime());
    }

    @Test
    public void shouldReturnList_WhenItMultiMatched() {
        List<Course> courseList = underTest.searchTerm("introduction");
        assertEquals(5, courseList.size());

        assertEquals("Introduction to mechanical ventilation", courseList.get(0).getCourseName());
        assertEquals(5, courseList.get(0).getCourseTime());

        assertEquals("Introduction to coronavirus disease 2019 (COVID-19)", courseList.get(1).getCourseName());
        assertEquals(3.5, courseList.get(1).getCourseTime());

        assertEquals("Quick tips: introduction to asthma", courseList.get(2).getCourseName());
        assertEquals(0, courseList.get(2).getCourseTime());

        assertEquals("Introduction to testing for COVID-19", courseList.get(3).getCourseName());
        assertEquals(7.5, courseList.get(3).getCourseTime());

        assertEquals("Quick tips: introduction to asthma", courseList.get(4).getCourseName());
        assertEquals(3.0, courseList.get(4).getCourseTime());

    }


    @Test
    public void shouldReturnCourseResult_WhenResultSingle() {
        Optional<CourseTotalTimeView> result = underTest.searchTermWithTotalTime("mechanical");
        assertTrue(result.isPresent());
        assertEquals(1, result.get().getCourses().size());
        assertEquals("Introduction to mechanical ventilation", result.get().getCourses().get(0).getCourseName());
        assertEquals(5.0, result.get().getTotalCourseTime());
    }

    @Test
    public void shouldReturnCourseResult_WhenResultMultiple() {
        Optional<CourseTotalTimeView> result = underTest.searchTermWithTotalTime("introduction");
        assertTrue(result.isPresent());
        assertEquals(5, result.get().getCourses().size());
        assertEquals(19.0, result.get().getTotalCourseTime());
    }

    @Test
    public void shouldReturnCourseResult_WhenResultContainsRemaindersDecimalPoints() {
        Optional<CourseTotalTimeView> result = underTest.searchTermWithTotalTime("quick");
        assertTrue(result.isPresent());
        assertEquals(2, result.get().getCourses().size());
        assertEquals(3.0, result.get().getTotalCourseTime());
    }

}
