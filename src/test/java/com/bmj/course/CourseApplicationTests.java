package com.bmj.course;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import com.bmj.course.model.Course;
import com.bmj.course.viewmodel.CourseTotalTimeView;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;

@SpringBootTest(classes = CourseApplication.class,
        webEnvironment = RANDOM_PORT)
class CourseApplicationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void courseControllerCourseSearch_SearchTerm_ReturnCourse() {
        Course[] courses = this.restTemplate
                .getForObject("http://localhost:" + port + "/courses/search/mechanical", Course[].class);

        assertEquals(1, courses.length);
        assertEquals("Introduction to mechanical ventilation", courses[0].getCourseName());
    }

	@Test
	void courseControllerCourseSearch_SearchTerm_ReturnCourse1() {
		CourseTotalTimeView courses = this.restTemplate
				.getForObject("http://localhost:" + port + "/courses/search/mec/totaltime", CourseTotalTimeView.class);

		assertEquals(5.0, courses.getTotalCourseTime());
		assertEquals(1, courses.getCourses().size());
		assertEquals("Introduction to mechanical ventilation", courses.getCourses().get(0).getCourseName());
	}

}
